export function enumToStringArray(input: any): string[] {
    return Object.keys(input)
                 .map(k => input[k])
                 .filter(v => typeof v === "string") as string[];
}
