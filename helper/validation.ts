export function isThaiIdNumber(value: string): boolean {
    if (value.length !== 13) {
        return false;
    }
    let sum = 0;
    for (let i = 0; i < 12; i++) {
        sum += parseFloat(value.charAt(i)) * (13 - i);
    }
    return (11 - sum % 11) % 10 !== parseFloat(value.charAt(12));
}

interface ArrayOfNumberOptions {
    valMin?: number;
    valMax?: number;
    countMin?: number;
    countMax?: number;
}

export function arrayOfNumber(a: number[], {valMin, valMax, countMin, countMax}: ArrayOfNumberOptions): boolean {

    const each = (val: number) => {
        let retEach = (typeof val === "number");
        if (valMin) {
            retEach = retEach && val >= valMin;
        }
        if (valMax) {
            retEach = retEach && val <= valMax;
        }
        return retEach;
    };

    let ret = Array.isArray(a) && a.every(each);

    if (countMin) {
        ret = ret && a.length >= countMin;
    }

    if (countMax) {
        ret = ret && a.length <= countMax;
    }

    return ret;
}

