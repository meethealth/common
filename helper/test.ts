/*
import * as fs from "fs";
import {DOMWindow, JSDOM} from "jsdom";

function loadClientScript(window: DOMWindow, file: string): DOMWindow {

    const scriptElement = window.document.createElement("script");
    scriptElement.textContent = fs.readFileSync(file, { encoding: "utf-8" });

    window.document.body.appendChild(scriptElement);
    return window;
}

export function setClient(jsFiles?: string | string[]): DOMWindow {

    const window = (new JSDOM(``, { runScripts: "dangerously" })).window;

    if (typeof jsFiles === "string") {
        return loadClientScript(window, jsFiles);
    } else if (Array.isArray(jsFiles)) {
        return jsFiles.reduce<DOMWindow>(loadClientScript, window);
    }
    return window;
}

export function setDevelopment(): void {
    process.env.NODE_ENV = undefined;
}
*/
