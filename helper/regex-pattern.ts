export const alphaThai = /^[\u0E01-\u0E4E]$/i;
export const alphaEngThai = /^[a-z\u0E01-\u0E4E ]$/i;
export const alphaNumEngThai = /^[a-z0-9\u0E01-\u0E59]$/i;
export const alphaNumEngThaiSpace = /^[a-z0-9\u0E01-\u0E59 ]$/i;
