export interface YearMonthDay {
    year?: number;
    month?: number;
    day?: number;
}

export function getAgeFromBirth(birth: Date | string): string | undefined {

    if (typeof birth === "string") {
        birth = new Date(birth);
    }
    if (!birth || !birth.getTime) {
        return undefined;
    }

    const birthTime = birth.getTime();
    if (isNaN(birthTime)) {
        return undefined;
    }

    return getStringFromYmd(getYmdFromBirth(birth));
}

function getYmdFromBirth(birth: Date): YearMonthDay {

    const now = new Date();

    let yearDif = now.getFullYear() - birth.getFullYear();

    const nowMonth = now.getMonth() + 1;
    const birthMonth = birth.getMonth() + 1;
    let monthDif;
    let dayDif;

    if (birthMonth > nowMonth) {
        yearDif--;
        monthDif = 12 - birthMonth + nowMonth;
    } else {
        monthDif = nowMonth - birthMonth;
    }

    if (yearDif <= 0) {
        // show days
        const nowDay = now.getDate();
        const birthDay = birth.getDate();

        if (birthDay > nowDay) {
            monthDif--;

            birth.setMonth(birth.getMonth() + 1);
            birth.setDate(0);
            const numberOfDaysInBirthMonth = birth.getDate();

            dayDif = numberOfDaysInBirthMonth - birthDay + nowDay;
        } else {
           dayDif = nowDay - birthDay;
        }
    }
    /*const ageDifMs = Date.now() - birthTime;
    const ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);*/

    return {
        year: yearDif,
        month: monthDif,
        day: dayDif,
    };
}

function getStringFromYmd(ymd: YearMonthDay, locale?: string): string {
    let ret = "";

    if (ymd.year) {
        ret += ymd.year + "y ";
    }
    if (ymd.month) {
        ret += ymd.month + "m ";
    }
    if (ymd.day) {
        ret += ymd.day + "d ";
    }

    if (ret === "") {
        ret = "0d";
    }

    return ret;
}
