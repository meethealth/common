import * as lolex from "lolex";
import * as DateTime from "./date-time";

describe("Date-Time helper function", () => {

    describe("Get age from birth as string", () => {

        let clock: lolex.InstalledClock;

        beforeAll(() => {
            clock = lolex.install({
                now: new Date("2018-1-15").getTime(),
                toFake: ["Date"],
            });
        });

        const testCases = [
            ["1960-8-10", "57y 5m"],
            ["1998-12-1", "19y 1m"],
            ["2017-1-18", "1y"],
            ["2017-2-18", "10m 25d"],
            ["2017-10-15", "3m"],
            ["2017-12-21", "25d"],
            ["2018-1-15", "0d"],
        ];

        testCases.forEach(each => {
            it("should return valid age as string", () => {
                const result = DateTime.getAgeFromBirth(each[0]);
                expect(result).toBeDefined();
                if (result) {
                    expect(result.trim()).toEqual(each[1]);
                }
            });
        });

        afterAll(() => {
            clock.uninstall();
        });
    });

});