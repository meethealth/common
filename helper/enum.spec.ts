import * as Module from "./enum";

describe("enumToStringArray", () => {

    it("should return array of string of all provided enum's key", () => {

        enum Test1 {}
        expect(Module.enumToStringArray(Test1)).toEqual([]);

        enum Test2 {""}
        expect(Module.enumToStringArray(Test2)).toEqual([""]);

        enum Test3 {a, b, c, DE, d}
        expect(Module.enumToStringArray(Test3)).toEqual(["a", "b", "c", "DE", "d"]);
    });

});
