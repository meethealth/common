export function extractPropsFromClass(obj: object): string[] {
    return Object.keys(obj).filter(name => {
        return typeof Object.getOwnPropertyDescriptor(obj, name) === "function";
    });
}
