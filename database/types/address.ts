type AddressKeys = "room" | "floor" | "number" | "building" | "alley" | "road" |
                   "district" | "subdistrict" | "province" | "city" | "postalCode";

export type Address = Record<AddressKeys | "all", string>;
