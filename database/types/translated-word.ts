import languageList from "../../helper/language-list";

const languageListArr = Object.keys(languageList);

export type TranslatedWord = {[T in (typeof languageListArr)[number]]: string};
