import {enumToStringArray} from "../../helper/enum";

export enum DoctorStatus {
    Active = "Active",
    Unverified = "Unverified", Pending = "Pending",
    Suspended = "Suspended", Removed = "Removed", Banned = "Banned",
}

export const DoctorStatusList = enumToStringArray(DoctorStatus);
