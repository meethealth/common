import {enumToStringArray} from "../../helper/enum";

export enum SpecialistSelectionAlgorithm {
    Manual = "Manual",
    Alternate = "Alternate", // สลับกัน

}

export const SpecialistSelectionAlgorithmList = enumToStringArray(SpecialistSelectionAlgorithm);
