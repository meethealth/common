import {enumToStringArray} from "../../helper/enum";

export enum ConsultItemType {
    Consult = "Consult",
    Refer = "Refer",
}

export const ConsultItemTypeList = enumToStringArray(ConsultItemType);
