import {enumToStringArray} from "../../helper/enum";

export enum DoctorRole {
    Consultee = "Consultee",
    Consultant = "Consultant",
    Both = "Both",
}

export const DoctorRoleList = enumToStringArray(DoctorRole);
