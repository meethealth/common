import {enumToStringArray} from "../../helper/enum";

export enum AccountType {
    Patient = "Patient", Doctor = "Doctor", Hospital = "Hospital", Organization = "Organization",
    Laboratory = "Laboratory", Insurance = "Insurance",
}

export const AccountTypeList = enumToStringArray(AccountType);
