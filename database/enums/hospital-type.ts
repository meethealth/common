import {enumToStringArray} from "../../helper/enum";

export enum HospitalType {
    Teaching = "Teaching", Regional = "Regional", General = "General", Community = "Community", PCU = "PCU",
    Chronic = "Chronic", Drug = "Drug", Infectious = "Infectious", Specialised = "Specialised",
    Clinic = "Clinic",
}

export const HospitalTypeList = enumToStringArray(HospitalType);
