import {enumToStringArray} from "../../helper/enum";

export enum ReimbursementMethod {
    Self = "Self", Internal = "Internal", Free = "Free",
}

export const ReimbursementMethodList = enumToStringArray(ReimbursementMethod);
