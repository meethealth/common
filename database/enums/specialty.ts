import {enumToStringArray} from "../../helper/enum";

export enum Specialty {
    GP = "GP", Derm = "Derm",

    Med = "Med", MedChest = "MedChest", MedCardio = "MedCardio", MedGI = "MedGI",
    MedPrevent = "MedPrevent", MedNeuro = "MedNeuro", MedFam = "MedFam",

    ObsGyn = "ObsGyn",

    Sx = "Sx", SxUro = "SxUro", SxCVT = "SxCVT", SxVas = "SxVas", SxNeuro = "SxNeuro",
    SxPed = "SxPed",

    Ped = "Ped", PedChest = "PedChest", PedAllergy = "PedAllergy", PedGI = "PedGI",
}

export const SpecialtyList = enumToStringArray(Specialty);
