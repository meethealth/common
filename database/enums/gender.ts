import {enumToStringArray} from "../../helper/enum";

export enum Gender {Male = "Male", Female = "Female"}

export const GenderList = enumToStringArray(Gender);
