import {enumToStringArray} from "../../helper/enum";

export enum PatientStatus {
    Active = "Active",
    Unverified = "Unverified", Pending = "Pending",
    Suspended = "Suspended", Removed = "Removed", Banned = "Banned",
    NoAccount = "NoAccount",
}

export const PatientStatusList = enumToStringArray(PatientStatus);
