import {HospitalType} from "../enums/hospital-type";
import {Address} from "../types/address";
import {TranslatedWord} from "../types/translated-word";
import {IConsultGroup} from "./IConsultGroup";
import {IPatient} from "./IPatient";
import {IPatientHospital} from "./IPatientHospital";

export {IConsultGroup, IPatient, IPatientHospital};

export interface IHospital {

    id?: number;

    name?: string;

    translatedName?: TranslatedWord;

    type?: HospitalType;

    isPrivate?: boolean;

    beds?: number;

    specialties?: string[];

    address?: Address;

    servicesId?: number[];

    createdAt?: Date;

    updatedAt?: Date;

    ///////////

    patientHospitals?: IPatientHospital[];

    patients?: IPatient[];

    consultGroups?: IConsultGroup[];

}
