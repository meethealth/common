import {Gender} from "../enums/gender";
import {PatientStatus} from "../enums/patient-status";
import {Address} from "../types/address";
import {IAccount} from "./IAccount";
import {IConsultItem} from "./IConsultItem";
import {IPatientHospital} from "./IPatientHospital";
import {IHospital} from "./IHospital";

export {IAccount, IConsultItem, IPatientHospital, IHospital};

export interface IPatient {

    id?: number;

    accountId?: number;
    account?: IAccount;

    firstName?: string;

    lastName?: string;

    idNumber?: string;

    HN?: any;

    birth?: Date;

    gender?: Gender;

    address?: Address;

    profilePictureUrl?: string;

    status?: PatientStatus;

    /////////////////

    consultItems?: IConsultItem[];

    patientHospitals?: IPatientHospital[];

    hospitals?: IHospital[];

}
