import {AccountType} from "../enums/account-type";
import {IDoctor} from "./IDoctor";
import {IPatient} from "./IPatient";

export {IDoctor, IPatient};

export interface IAccount {

    id?: number;

    email?: string;

    fbId?: string;

    password?: string;

    type?: AccountType;

    balance?: number;

    notiToken?: string;

    lastLoginAt?: Date;

    createdAt?: Date;


    doctor?: IDoctor;

    patient?: IPatient;

}
