import {ReimbursementMethod} from "../enums/reimburstment-method";
import {SpecialistSelectionAlgorithm} from "../enums/specialist-selection-algorithm";
import {Address} from "../types/address";
import {IConsultItem} from "./IConsultItem";
import {IDoctor} from "./IDoctor";
import {IHospital} from "./IHospital";

export {IConsultItem, IDoctor, IHospital};

export interface IConsultGroup {

    id?: number;

    scope?: Partial<Address>;

    specialtyId?: string;

    specialistSelectionAlgorithm?: SpecialistSelectionAlgorithm;

    pictureUploadUrl?: string;

    reimbursement?: ReimbursementMethod;

    isPrivate?: boolean;

    createdAt?: Date;

    /////

    consultItems?: IConsultItem[];

    doctors?: IDoctor[];

    hospitals?: IHospital[];
}
