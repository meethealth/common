import {IConsultGroup} from "./IConsultGroup";
import {IConsultItem} from "./IConsultItem";

export {IConsultGroup, IConsultItem};

export interface ISpecialty {

    id?: string;

    parentId?: string;
    parent?: ISpecialty;

    description?: string;

    alias?: string[];

    children?: ISpecialty[];

    consultGroup?: IConsultGroup[];
    consultItem?: IConsultItem[];

    // @TODO: Has Many Doctor

}
