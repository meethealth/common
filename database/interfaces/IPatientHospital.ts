import {IPatient} from "./IPatient";
import {IHospital} from "./IHospital";

export {IHospital, IPatient};

export interface IPatientHospital {

    id?: number;

    patientId?: number

    hospitalId?: number

    HN?: string;

    patients?: IPatient[];
    hospitals?: IHospital[];

}