import {IBaseMediaMeta} from "./IBaseMediaMeta";

export interface IPhotoMeta extends IBaseMediaMeta {

    rotation?: number;

}
