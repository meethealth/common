import {IBaseMediaMeta} from "./IBaseMediaMeta";

export interface IMriMeta extends IBaseMediaMeta {

    seriesNumber?: number;

}
