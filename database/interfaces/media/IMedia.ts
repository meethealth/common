import {ICtMeta} from "./ICtMeta";
import {IMicroscopeMeta} from "./IMicroscopeMeta";
import {IMriMeta} from "./IMriMeta";
import {IPhotoMeta} from "./IPhotoMeta";
import {IUltrasoundMeta} from "./IUltrasoundMeta";
import {IVideoMeta} from "./IVideoMeta";

export interface IMedia {

    photo?: IPhotoMeta[];

    video?: IVideoMeta[];

    microscope?: IMicroscopeMeta[];

    ultrasound?: IUltrasoundMeta[],

    CT?: ICtMeta[];

    MRI?: IMriMeta[];

}
