import {IBaseMediaMeta} from "./IBaseMediaMeta";

export interface IVideoMeta extends IBaseMediaMeta {

    rotation?: number;

    duration?: number;

    framerate?: number;

    bitrate?: number;

}
