import {IBaseMediaMeta} from "./IBaseMediaMeta";

export interface IMicroscopeMeta extends IBaseMediaMeta {

    magnifications?: number[];

}
