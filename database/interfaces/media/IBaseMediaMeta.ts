export interface IBaseMediaMeta {

    url: string;

    mime?: string;

    desc?: string;

    w?: number;

    h?: number;

    institution?: string;

    studyDate?: Date;

    modifiedDate?: Date;

}
