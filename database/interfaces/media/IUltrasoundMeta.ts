import {IBaseMediaMeta} from "./IBaseMediaMeta";

export interface IUltrasoundMeta extends IBaseMediaMeta {

    seriesNumber?: number;

}
