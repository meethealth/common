import {ConsultItemType} from "../enums/consult-item-type";
import {IMedia} from "./media/IMedia";
import {IConsultGroup} from "./IConsultGroup";
import {IDoctor} from "./IDoctor";
import {IPatient} from "./IPatient";
import {ISpecialty} from "./ISpecialty";
import {IConsultDiscussion} from "./IConsultDiscussion";

export {IConsultGroup, IDoctor, IPatient};

export interface IConsultItem {

    id?: number;

    type?: ConsultItemType;

    consultGroupId?: number;

    consulteeDoctorId?: number;

    /**
     * Set only when having intention to specify doctor for consultation
     */
    targetDoctorId?: number;

    patientId?: number;

    specialtyId?: string;

    compensationAmount?: number;

    chiefComplaint?: string;

    timing?: number; // Seconds

    priority?: number;

    history?: any;

    examination?: any;

    media?: IMedia;

    investigation?: any;

    provisionalDx?: any;

    otherData?: any;

    /// Answer ///

    isAnswered?: boolean;

    finding?: any;

    impression?: any;

    recommendation?: any;

    createdAt?: Date;

    updatedAt?: Date;

    //////

    consultGroup?: IConsultGroup;

    consulteeDoctor?: IDoctor;

    targetDoctor?: IDoctor;

    patient?: IPatient;

    specialty?: ISpecialty;

    consultDiscussions?: IConsultDiscussion[];

}
