import {DoctorStatus} from "../enums/doctor-status";
import {Gender} from "../enums/gender";
import {IAccount} from "./IAccount";
import {IConsultItem} from "./IConsultItem";
import {IDoctorConsultGroup} from "./IDoctorConsultGroup";
import {IConsultGroup} from "./IConsultGroup";

export {IAccount, IConsultItem, IDoctorConsultGroup, IConsultGroup};

export interface IDoctor {

    id?: number;

    accountId?: number;

    firstName?: string;

    lastName?: string;

    idNumber?: string;

    licenceNumber?: string;

    licencePictureUrl?: string;

    birth?: Date;

    gender?: Gender;

    profilePictureUrl?: string;

    specialties?: string[];

    hospitalId?: number[];

    status?: DoctorStatus;

    doVirtualVisit?: boolean;

    consulteeGroupId?: number[];

    consultantGroupId?: number[];

    ////

    account?: IAccount;
    consultsMade?: IConsultItem[];
    consultsGet?: IConsultItem[];
    doctorConsultGroups?: IDoctorConsultGroup[];
    consultGroups?: IConsultGroup[];

}
