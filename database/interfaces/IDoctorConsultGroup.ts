import {DoctorRole} from "../enums/doctor-role";
import {IDoctor} from "./IDoctor";
import {IConsultGroup} from "./IConsultGroup";

export {IConsultGroup, IDoctor};

export interface IDoctorConsultGroup {

    id?: number;

    doctorId?: number;

    consultGroupId?: number;

    role: DoctorRole;

    doctors?: IDoctor[];
    consultGroups?: IConsultGroup[];

}