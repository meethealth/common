import {IMedia} from "./media/IMedia";
import {IConsultItem} from "./IConsultItem";

export interface IConsultDiscussion {

    consultItemId?: number;

    content?: string;

    media?: IMedia;

    parentId?: number;

    createdAt?: Date;

    /////

    parent?: IConsultDiscussion;

    children?: IConsultDiscussion[];

    /////

    consultItem?: IConsultItem;
}
