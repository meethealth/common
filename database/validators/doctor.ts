import {isISO8601, isIn, isInt, isLength, isURL} from "validator";
import {arrayOfNumber, isThaiIdNumber} from "../../helper/validation";
import {DoctorStatusList} from "../enums/doctor-status";
import {GenderList} from "../enums/gender";
import SpecialtyValidator from "./specialty";

export default {

    firstName: (a: string) => isLength(a, { min: 1, max: 256 }),

    lastName: (a: string) => isLength(a, { min: 1, max: 256 }),

    idNumber: (a: string) => !a || isInt(a) && isThaiIdNumber(a),

    licenceNumber: (a: string) => !a || isInt(a) && isLength(a, { min: 1, max: 10 }),

    licencePictureUrl: (a?: string) => !a || (isURL(a) && a.length <= 2048),

    birth: (a: string) => isISO8601(a),

    gender: (a: string) => isIn(a, GenderList),

    profilePictureUrl: (a: string) => !a || isURL(a),

    specialties: (a: string[]) => a && Array.isArray(a)
                                    && a.length <= 128
                                    && a.every(SpecialtyValidator.id),

    hospitalId: (a?: number[]) => !a || arrayOfNumber(a, { valMin: 1, countMax: 40 }),

    status: (a?: string) => !a || isIn(a, DoctorStatusList),

    doVirtualVisit: (a?: boolean) => (typeof a === "boolean") || (a === undefined) || (a === null),

    consulteeGroupId: (a?: number[]) => !a || (
        arrayOfNumber(a, { valMin: 1, countMax: 200 })
    ),

    consultantGroupId: (a?: number[]) => !a || (
        arrayOfNumber(a, { valMin: 1, countMax: 200 })
    ),

};
