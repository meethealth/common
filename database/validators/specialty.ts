import {isAlpha, isLength} from "validator";

export default {

    id: (a: string) => isAlpha(a) && isLength(a, { min: 1, max: 256 }),
    description: (a: string) => !a || isAlpha(a) && isLength(a, { min: 1, max: 256 }),
    alias: (a: string[]) => !a || Array.isArray(a) && a.map(each =>
        isAlpha(each) && isLength(each, { min: 1, max: 256 })
    ),

};
