import {isISO8601, isIn, isInt, isLength, isURL} from "validator";
import {isThaiIdNumber} from "../../helper/validation";
import {GenderList} from "../enums/gender";
import {address} from "./common";

export default {

    firstName: (a: string) => isLength(a, { min: 1, max: 256 }),

    lastName: (a: string) => isLength(a, { min: 1, max: 256 }),

    idNumber: (a: string) => !a || isInt(a) && isThaiIdNumber(a),

    birth: (a: string) => isISO8601(a),

    gender: (a: string) => isIn(a, GenderList),

    address: (a: string) => !a || address(a),

    profilePictureUrl: (a?: string) => !a || isURL(a),

};
