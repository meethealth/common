import {isIn, isLength} from "validator";
import languageList from "../../helper/language-list";
import * as regEx from "../../helper/regex-pattern";

export function address(a: any) {
    return a
        && Object.keys(a).every(each =>
            new RegExp(regEx.alphaNumEngThaiSpace).test(each) && isLength(each, {min: 2, max: 100})
        );
}

export function translatedWord(a: any) {
    const languageListArr = Object.keys(languageList);
    return a
        && Object.keys(a).every(
            key => (
                (languageListArr.indexOf(key) >= 0) && (typeof a[key] === "string")
            )
        );
}
