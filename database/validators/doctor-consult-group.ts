import {DoctorRoleList} from "../enums/doctor-role";

export default {

    role: (a: string) => DoctorRoleList.indexOf(a),

}
