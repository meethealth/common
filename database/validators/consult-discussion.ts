import {isLength, isIn, isInt} from "validator";

export default {

    content: (a: string) => !a || isLength(a, { min: 2, max: 510 }),

    media: (a: string) => !a || Object.keys(a).length > 0,

};
