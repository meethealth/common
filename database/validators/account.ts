import {isEmail, isIn, isLength} from "validator";
import {AccountTypeList} from "../enums/account-type";

export const MAX_PASSWORD_LENGTH = 128;

export default  {

    email: (a: string) => isEmail(a),

    fbId: (a?: string) => !a || isLength(a, { max: 1024 }),

    password: (a: string) => !a || new RegExp("^[a-z0-9\.\-_]{8," + MAX_PASSWORD_LENGTH + "}$/i").test(a),

    type: (a: string) => isIn(a, AccountTypeList),

    balance: (a: number) => (typeof a === "number") && (a >= 0) && (a <= 9999999),

    notiToken: (a: string) => a.length ? a.length < 500 : true, // @TODO: implement based on noti service provider

};
