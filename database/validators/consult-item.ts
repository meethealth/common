import {isIn, isLength} from "validator";
import SpecialtyValidator from "./specialty";
import {ConsultItemTypeList} from "../enums/consult-item-type";

export default {

    specialtyId: SpecialtyValidator.id,

    type: (a: any) => isIn(a, ConsultItemTypeList),

    compensationAmount: (a: number) => !a || ((typeof a === "number") && (a >= 0) && (a <= 99999)),

    chiefComplaint: (a: string) => {console.log(a.length>2 && a.length<64);return a.length>2 && a.length<64;/*isLength(a, {min: 2, max: 64})*/},

    timing: (a?: number) => !a || ((typeof a === "number") && (a >= 1) && (a <= 3000000000)),

    priority: (a?: number) => !a || ((typeof a === "number") && (a >= 1) && (a <= 3000000000)),

    isAnswered: (a?: boolean) => a === undefined || a === true || a === false,

};
