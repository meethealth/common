import {isInt} from "validator";

export default {

    HN: (a: string) => isInt(a),

};
