import {isIn, isLength} from "validator";
import * as regEx from "../../helper/regex-pattern";
import {HospitalTypeList} from "../enums/hospital-type";
import {address, translatedWord} from "./common";
import SpecialtyValidator from "./specialty";

export default {

    name: (a: any) => new RegExp(regEx.alphaNumEngThaiSpace).test(a) && isLength(a, {min: 2, max: 256}),

    translatedName: (a?: any) => !a || translatedWord(a), // validate key as language code

    type: (a: any) => isIn(a, HospitalTypeList),

    isPrivate: (a: any) => (typeof a === "boolean"),

    beds: (a: any) => (typeof a === "number") && a >= 0,

    specialties: (a: any) => a && Array.isArray(a)
        && a.length <= 128
        && a.every(SpecialtyValidator.id),

    address: (a: any) => !a || address(a),

    /*servicesId: (a?: string) => !a || (

    ),*/

};
