import {isAlpha, isIn, isLength, isURL} from "validator";
import {ReimbursementMethodList} from "../enums/reimburstment-method";
import {SpecialistSelectionAlgorithmList} from "../enums/specialist-selection-algorithm";
import SpecialtyValidator from "./specialty";

export default {

    specialtyId: SpecialtyValidator.id,

    specialistSelectionAlgorithm: (a: string) => isIn(a, SpecialistSelectionAlgorithmList),

    pictureUploadUrl: (a?: string) => !a || isURL(a),

    reimbursement: (a: string) => isIn(a, ReimbursementMethodList),

    isPrivate: (a?: boolean) => a === undefined || a === true || a === false,

};
