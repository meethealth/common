"use strict";
// @Warning: File will be published, DO NOT place app secret here
Object.defineProperty(exports, "__esModule", { value: true });
exports.fb = {
    sdkUrl: "//connect.facebook.net/en_US/sdk.js",
    MeetDoc: {
        appId: "224493068044006",
        autoLogAppEvents: true,
        xfbml: true,
        version: "v2.9",
    },
    MeetDoc_Test_Staging: {
        appId: "711003519089116",
        autoLogAppEvents: true,
        xfbml: true,
        version: "v2.9",
    },
    MeetDoc_Test_Local: {
        appId: "136944810390496",
        autoLogAppEvents: true,
        xfbml: true,
        version: "v2.9",
    },
};
exports.opentok = {
    MeetDoc: {
        apiKey: "45878912",
        name: "MeetDoc",
    },
};
//# sourceMappingURL=api.js.map