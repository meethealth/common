"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Result;
(function (Result) {
    Result["Success"] = "Success";
    Result["NotFound"] = "NotFound";
    Result["Unauthorized"] = "Unauthorized";
    Result["InputError"] = "InputError";
    Result["AppError"] = "AppError";
})(Result = exports.Result || (exports.Result = {}));
//# sourceMappingURL=IResponse.js.map