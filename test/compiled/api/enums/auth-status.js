"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var AuthStatus;
(function (AuthStatus) {
    AuthStatus["Full"] = "Full";
    AuthStatus["ViewOnly"] = "ViewOnly";
    AuthStatus["Guest"] = "Guest";
})(AuthStatus = exports.AuthStatus || (exports.AuthStatus = {}));
//# sourceMappingURL=auth-status.js.map