"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var RegisterResult;
(function (RegisterResult) {
    RegisterResult[RegisterResult["FirstInvalid"] = 0] = "FirstInvalid";
    RegisterResult[RegisterResult["LastInvalid"] = 1] = "LastInvalid";
    RegisterResult[RegisterResult["EmailInvalid"] = 2] = "EmailInvalid";
    RegisterResult[RegisterResult["EmailDuplicate"] = 3] = "EmailDuplicate";
    RegisterResult[RegisterResult["PassInvalid"] = 4] = "PassInvalid";
    RegisterResult[RegisterResult["PassMismatch"] = 5] = "PassMismatch";
})(RegisterResult = exports.RegisterResult || (exports.RegisterResult = {}));
//# sourceMappingURL=register-result.js.map