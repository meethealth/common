"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Validation = /** @class */ (function () {
    function Validation() {
    }
    Validation.isThaiIdNumber = function (value) {
        if (value.length !== 13) {
            return false;
        }
        var sum = 0;
        for (var i = 0; i < 12; i++) {
            sum += parseFloat(value.charAt(i)) * (13 - i);
        }
        return (11 - sum % 11) % 10 !== parseFloat(value.charAt(12));
    };
    Validation.arrayOfNumber = function (a, _a) {
        var valMin = _a.valMin, valMax = _a.valMax, countMin = _a.countMin, countMax = _a.countMax;
        var each = function (val) {
            var retEach = (typeof val === "number");
            if (valMin) {
                retEach = retEach && val >= valMin;
            }
            if (valMax) {
                retEach = retEach && val <= valMax;
            }
            return retEach;
        };
        var ret = Array.isArray(a) && a.every(each);
        if (countMin) {
            ret = ret && a.length >= countMin;
        }
        if (countMax) {
            ret = ret && a.length <= countMax;
        }
        return ret;
    };
    return Validation;
}());
exports.Validation = Validation;
exports.default = Validation;
//# sourceMappingURL=validation.js.map