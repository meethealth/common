"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
var Module = require("./enum");
describe("enumToStringArray", function () {
    it("should return array of string of all provided enum's key", function () {
        var Test1;
        (function (Test1) {
        })(Test1 || (Test1 = {}));
        chai_1.expect(Module.enumToStringArray(Test1)).to.eql([]);
        var Test2;
        (function (Test2) {
            Test2[Test2[""] = 0] = "";
        })(Test2 || (Test2 = {}));
        chai_1.expect(Module.enumToStringArray(Test2)).to.eql([""]);
        var Test3;
        (function (Test3) {
            Test3[Test3["a"] = 0] = "a";
            Test3[Test3["b"] = 1] = "b";
            Test3[Test3["c"] = 2] = "c";
            Test3[Test3["DE"] = 3] = "DE";
            Test3[Test3["d"] = 4] = "d";
        })(Test3 || (Test3 = {}));
        chai_1.expect(Module.enumToStringArray(Test3)).to.eql(["a", "b", "c", "DE", "d"]);
    });
});
//# sourceMappingURL=enum.spec.js.map