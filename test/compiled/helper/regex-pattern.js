"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.alphaThai = /^[\u0E01-\u0E4E]$/i;
exports.alphaEngThai = /^[a-z\u0E01-\u0E4E ]$/i;
exports.alphaNumEngThai = /^[a-z0-9\u0E01-\u0E59]$/i;
exports.alphaNumEngThaiSpace = /^[a-z0-9\u0E01-\u0E59 ]$/i;
//# sourceMappingURL=regex-pattern.js.map