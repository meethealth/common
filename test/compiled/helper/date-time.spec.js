"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var chai_1 = require("chai");
var sinon = require("sinon");
var DateTime = require("./date-time");
describe("Date-Time helper function", function () {
    describe("Get age from birth as string", function () {
        var clock;
        before(function () {
            clock = sinon.useFakeTimers(new Date("2018-1-15").getTime());
        });
        var testCases = [
            ["1960-8-10", "57y 5m"],
            ["1998-12-1", "19y 1m"],
            ["2017-1-18", "1y"],
            ["2017-2-18", "10m 25d"],
            ["2017-10-15", "3m"],
            ["2017-12-21", "25d"],
            ["2018-1-15", "0d"],
        ];
        testCases.forEach(function (each) {
            it("should return valid age as string", function () {
                var result = DateTime.getAgeFromBirth(each[0]);
                chai_1.expect(result).to.not.be.undefined;
                chai_1.expect(result.trim()).to.eql(each[1]);
            });
        });
        after(function () {
            clock.restore();
        });
    });
});
//# sourceMappingURL=date-time.spec.js.map