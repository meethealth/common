"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function enumToStringArray(input) {
    return Object.keys(input)
        .map(function (k) { return input[k]; })
        .filter(function (v) { return typeof v === "string"; });
}
exports.enumToStringArray = enumToStringArray;
//# sourceMappingURL=enum.js.map