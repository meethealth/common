"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function extractPropsFromClass(obj) {
    return Object.keys(obj).filter(function (name) {
        return typeof Object.getOwnPropertyDescriptor(obj, name) === "function";
    });
}
exports.extractPropsFromClass = extractPropsFromClass;
//# sourceMappingURL=extract-props-from-class.js.map