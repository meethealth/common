"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
function getAgeFromBirth(birth) {
    if (typeof birth === "string") {
        birth = new Date(birth);
    }
    if (!birth || !birth.getTime) {
        return undefined;
    }
    var birthTime = birth.getTime();
    if (isNaN(birthTime)) {
        return undefined;
    }
    return getStringFromYmd(getYmdFromBirth(birth));
}
exports.getAgeFromBirth = getAgeFromBirth;
function getYmdFromBirth(birth) {
    var now = new Date();
    var yearDif = now.getFullYear() - birth.getFullYear();
    var nowMonth = now.getMonth() + 1;
    var birthMonth = birth.getMonth() + 1;
    var monthDif;
    var dayDif;
    if (birthMonth > nowMonth) {
        yearDif--;
        monthDif = 12 - birthMonth + nowMonth;
    }
    else {
        monthDif = nowMonth - birthMonth;
    }
    if (yearDif <= 0) {
        // show days
        var nowDay = now.getDate();
        var birthDay = birth.getDate();
        if (birthDay > nowDay) {
            monthDif--;
            birth.setMonth(birth.getMonth() + 1);
            birth.setDate(0);
            var numberOfDaysInBirthMonth = birth.getDate();
            dayDif = numberOfDaysInBirthMonth - birthDay + nowDay;
        }
        else {
            dayDif = nowDay - birthDay;
        }
    }
    /*const ageDifMs = Date.now() - birthTime;
    const ageDate = new Date(ageDifMs); // miliseconds from epoch
    return Math.abs(ageDate.getUTCFullYear() - 1970);*/
    return {
        year: yearDif,
        month: monthDif,
        day: dayDif,
    };
}
function getStringFromYmd(ymd, locale) {
    var ret = "";
    if (ymd.year) {
        ret += ymd.year + "y ";
    }
    if (ymd.month) {
        ret += ymd.month + "m ";
    }
    if (ymd.day) {
        ret += ymd.day + "d ";
    }
    if (ret === "") {
        ret = "0d";
    }
    return ret;
}
//# sourceMappingURL=date-time.js.map