"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var enum_1 = require("../../helper/enum");
var PatientStatus;
(function (PatientStatus) {
    PatientStatus["Active"] = "Active";
    PatientStatus["Unverified"] = "Unverified";
    PatientStatus["Pending"] = "Pending";
    PatientStatus["Suspended"] = "Suspended";
    PatientStatus["Removed"] = "Removed";
    PatientStatus["Banned"] = "Banned";
})(PatientStatus = exports.PatientStatus || (exports.PatientStatus = {}));
exports.PatientStatusList = enum_1.enumToStringArray(PatientStatus);
//# sourceMappingURL=patient-status.js.map