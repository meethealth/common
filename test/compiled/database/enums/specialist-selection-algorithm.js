"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var enum_1 = require("../../helper/enum");
var SpecialistSelectionAlgorithm;
(function (SpecialistSelectionAlgorithm) {
    SpecialistSelectionAlgorithm["Manual"] = "Manual";
    SpecialistSelectionAlgorithm["Alternate"] = "Alternate";
})(SpecialistSelectionAlgorithm = exports.SpecialistSelectionAlgorithm || (exports.SpecialistSelectionAlgorithm = {}));
exports.SpecialistSelectionAlgorithmList = enum_1.enumToStringArray(SpecialistSelectionAlgorithm);
//# sourceMappingURL=specialist-selection-algorithm.js.map