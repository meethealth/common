"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var enum_1 = require("../../helper/enum");
var HospitalType;
(function (HospitalType) {
    HospitalType["Teaching"] = "Teaching";
    HospitalType["Regional"] = "Regional";
    HospitalType["General"] = "General";
    HospitalType["Community"] = "Community";
    HospitalType["PCU"] = "PCU";
    HospitalType["Chronic"] = "Chronic";
    HospitalType["Drug"] = "Drug";
    HospitalType["Infectious"] = "Infectious";
    HospitalType["Specialised"] = "Specialised";
    HospitalType["Clinic"] = "Clinic";
})(HospitalType = exports.HospitalType || (exports.HospitalType = {}));
exports.HospitalTypeList = enum_1.enumToStringArray(HospitalType);
//# sourceMappingURL=hospital-type.js.map