"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var enum_1 = require("../../helper/enum");
var ReimbursementMethod;
(function (ReimbursementMethod) {
    ReimbursementMethod["Self"] = "Self";
    ReimbursementMethod["Internal"] = "Internal";
})(ReimbursementMethod = exports.ReimbursementMethod || (exports.ReimbursementMethod = {}));
exports.ReimbursementMethodList = enum_1.enumToStringArray(ReimbursementMethod);
//# sourceMappingURL=reimburstment-method.js.map