"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var enum_1 = require("../../helper/enum");
var Specialty;
(function (Specialty) {
    Specialty["GP"] = "GP";
    Specialty["Derm"] = "Derm";
    Specialty["Med"] = "Med";
    Specialty["MedChest"] = "MedChest";
    Specialty["MedCardio"] = "MedCardio";
    Specialty["MedGI"] = "MedGI";
    Specialty["MedPrevent"] = "MedPrevent";
    Specialty["MedNeuro"] = "MedNeuro";
    Specialty["MedFam"] = "MedFam";
    Specialty["ObsGyn"] = "ObsGyn";
    Specialty["Sx"] = "Sx";
    Specialty["SxUro"] = "SxUro";
    Specialty["SxCVT"] = "SxCVT";
    Specialty["SxVas"] = "SxVas";
    Specialty["SxNeuro"] = "SxNeuro";
    Specialty["SxPed"] = "SxPed";
    Specialty["Ped"] = "Ped";
    Specialty["PedChest"] = "PedChest";
    Specialty["PedAllergy"] = "PedAllergy";
    Specialty["PedGI"] = "PedGI";
})(Specialty = exports.Specialty || (exports.Specialty = {}));
exports.SpecialtyList = enum_1.enumToStringArray(Specialty);
//# sourceMappingURL=specialty.js.map