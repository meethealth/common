"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var enum_1 = require("../../helper/enum");
var Gender;
(function (Gender) {
    Gender["Male"] = "Male";
    Gender["Female"] = "Female";
})(Gender = exports.Gender || (exports.Gender = {}));
exports.GenderList = enum_1.enumToStringArray(Gender);
//# sourceMappingURL=gender.js.map