"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var enum_1 = require("../../helper/enum");
var AccountType;
(function (AccountType) {
    AccountType["Patient"] = "Patient";
    AccountType["Doctor"] = "Doctor";
    AccountType["Hospital"] = "Hospital";
    AccountType["Organization"] = "Organization";
    AccountType["Laboratory"] = "Laboratory";
    AccountType["Insurance"] = "Insurance";
})(AccountType = exports.AccountType || (exports.AccountType = {}));
exports.AccountTypeList = enum_1.enumToStringArray(AccountType);
//# sourceMappingURL=account-type.js.map