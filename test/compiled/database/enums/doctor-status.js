"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var enum_1 = require("../../helper/enum");
var DoctorStatus;
(function (DoctorStatus) {
    DoctorStatus["Active"] = "Active";
    DoctorStatus["Unverified"] = "Unverified";
    DoctorStatus["Pending"] = "Pending";
    DoctorStatus["Suspended"] = "Suspended";
    DoctorStatus["Removed"] = "Removed";
    DoctorStatus["Banned"] = "Banned";
})(DoctorStatus = exports.DoctorStatus || (exports.DoctorStatus = {}));
exports.DoctorStatusList = enum_1.enumToStringArray(DoctorStatus);
//# sourceMappingURL=doctor-status.js.map