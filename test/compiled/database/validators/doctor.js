"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validator_1 = require("validator");
var validation_1 = require("../../helper/validation");
var doctor_status_1 = require("../enums/doctor-status");
var gender_1 = require("../enums/gender");
var specialty_1 = require("./specialty");
exports.default = {
    firstName: function (a) { return validator_1.isLength(a, { min: 1, max: 256 }); },
    lastName: function (a) { return validator_1.isLength(a, { min: 1, max: 256 }); },
    idNumber: function (a) { return validator_1.isInt(a) && validation_1.Validation.isThaiIdNumber(a); },
    licenceNumber: function (a) { return validator_1.isInt(a) && validator_1.isLength(a, { min: 1, max: 10 }); },
    licencePictureUrl: function (a) { return !a || (validator_1.isURL(a) && a.length <= 2048); },
    birth: function (a) { return validator_1.isISO8601(a); },
    gender: function (a) { return validator_1.isIn(a, gender_1.GenderList); },
    profilePictureUrl: function (a) { return validator_1.isURL(a); },
    specialties: function (a) { return a && Array.isArray(a)
        && a.length <= 128
        && a.every(specialty_1.default.id); },
    hospitalId: function (a) { return !a || validation_1.Validation.arrayOfNumber(a, { valMin: 1, countMax: 40 }); },
    status: function (a) { return !a || validator_1.isIn(a, doctor_status_1.DoctorStatusList); },
    doVirtualVisit: function (a) { return (typeof a === "boolean") || (a === undefined) || (a === null); },
    consulteeGroupId: function (a) { return !a || (validation_1.Validation.arrayOfNumber(a, { valMin: 1, countMax: 200 })); },
    consultantGroupId: function (a) { return !a || (validation_1.Validation.arrayOfNumber(a, { valMin: 1, countMax: 200 })); },
};
//# sourceMappingURL=doctor.js.map