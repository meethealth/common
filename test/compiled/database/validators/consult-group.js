"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validator_1 = require("validator");
var reimburstment_method_1 = require("../enums/reimburstment-method");
var specialist_selection_algorithm_1 = require("../enums/specialist-selection-algorithm");
var specialty_1 = require("./specialty");
exports.default = {
    specialty: specialty_1.default.id,
    specialistSelectionAlgorithm: function (a) { return validator_1.isIn(a, specialist_selection_algorithm_1.SpecialistSelectionAlgorithmList); },
    pictureUploadUrl: function (a) { return !a || validator_1.isURL(a); },
    reimbursement: function (a) { return validator_1.isIn(a, reimburstment_method_1.ReimbursementMethodList); },
};
//# sourceMappingURL=consult-group.js.map