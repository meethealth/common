"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validator_1 = require("validator");
exports.default = {
    id: function (a) { return validator_1.isAlpha(a) && validator_1.isLength(a, { min: 1, max: 256 }); },
};
//# sourceMappingURL=specialty.js.map