"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validator_1 = require("validator");
var account_type_1 = require("../enums/account-type");
exports.MAX_PASSWORD_LENGTH = 128;
exports.default = {
    email: function (a) { return validator_1.isEmail(a); },
    fbId: function (a) { return !a || validator_1.isLength(a, { max: 1024 }); },
    password: function (a) { return new RegExp("^[a-z0-9\.\-_]{8," + exports.MAX_PASSWORD_LENGTH + "}$/i").test(a); },
    type: function (a) { return validator_1.isIn(a, account_type_1.AccountTypeList); },
    balance: function (a) { return (typeof a === "number") && (a >= -987654321) && (a <= 987654321098); },
};
//# sourceMappingURL=account.js.map