"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var specialty_1 = require("./specialty");
exports.default = {
    specialty: specialty_1.default.id,
    chiefComplaint: function (a) { console.log(a.length > 2 && a.length < 64); return a.length > 2 && a.length < 64; /*isLength(a, {min: 2, max: 64})*/ },
    timing: function (a) { return !a || ((typeof a === "number") && (a >= 1) && (a <= 3000000000)); },
    isAnswered: function (a) { return a === undefined || a === true || a === false; },
};
//# sourceMappingURL=consult-item.js.map