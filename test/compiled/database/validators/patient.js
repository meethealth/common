"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validator_1 = require("validator");
var validation_1 = require("../../helper/validation");
var gender_1 = require("../enums/gender");
exports.default = {
    firstName: function (a) { return validator_1.isLength(a, { min: 1, max: 256 }); },
    lastName: function (a) { return validator_1.isLength(a, { min: 1, max: 256 }); },
    idNumber: function (a) { return validator_1.isInt(a) && validation_1.Validation.isThaiIdNumber(a); },
    birth: function (a) { return validator_1.isISO8601(a); },
    gender: function (a) { return validator_1.isIn(a, gender_1.GenderList); },
    profilePictureUrl: function (a) { return !a || validator_1.isURL(a); },
};
//# sourceMappingURL=patient.js.map