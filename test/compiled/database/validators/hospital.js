"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var validator_1 = require("validator");
var regEx = require("../../helper/regex-pattern");
var hospital_type_1 = require("../enums/hospital-type");
var specialty_1 = require("./specialty");
exports.default = {
    name: function (a) { return new RegExp(regEx.alphaNumEngThaiSpace).test(a) && validator_1.isLength(a, { min: 2, max: 256 }); },
    translatedName: function (a) { return true; },
    type: function (a) { return validator_1.isIn(a, hospital_type_1.HospitalTypeList); },
    isPrivate: function (a) { return (typeof a === "boolean"); },
    beds: function (a) { return (typeof a === "number") && a >= 0; },
    specialties: function (a) { return a && Array.isArray(a)
        && a.length <= 128
        && a.every(specialty_1.default.id); },
    province: function (a) { return new RegExp(regEx.alphaEngThai).test(a) && validator_1.isLength(a, { min: 2, max: 100 }); },
    district: this.province,
    subdistrict: this.province,
    address: function (a) { return !a || (new RegExp(regEx.alphaNumEngThaiSpace).test(a) && validator_1.isLength(a, { min: 2, max: 500 })); },
};
//# sourceMappingURL=hospital.js.map