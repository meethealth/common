import {IConsultItem} from "../../database/interfaces/IConsultItem";

export type AllConsultsData = [IConsultItem[], IConsultItem[]];
