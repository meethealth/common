export enum RegisterResult {
    FirstInvalid, LastInvalid,
    EmailInvalid, EmailDuplicate,
    PassInvalid, PassMismatch,
}
