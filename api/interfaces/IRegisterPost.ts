export interface IRegisterPost {
    email: string;
    password: string;
}

export interface IRegisterPatientPost extends IRegisterPost {
    firstName: string;
    lastName: string;
}

export interface IRegisterDoctorPost extends IRegisterPost {
    firstName: string;
    lastName: string;
}
