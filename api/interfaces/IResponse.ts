export enum Result {
    Success = "Success", NotFound = "NotFound", Unauthorized = "Unauthorized",
    InputError = "InputError", AppError = "AppError",
}

export interface IResponseError<codeT = string> {
    code: codeT;
    title?: string;
    message?: string;
    exception?: Error;
}

export interface IResponse<dataT = any, errorT = string> {
    error: IResponseError<errorT>;
    data?: dataT;
    result: Result;
    isSuccess: () => boolean;
    isNotFound: () => boolean;
    isUnauthorized: () => boolean;
    isAppError: () => boolean;
    isInputError: () => boolean;
}
