import {IAccount} from "../../database/interfaces/IAccount";

export interface FirebaseLogin {
    token?: string,
    error?: string,
}

export interface ILoginReturn {
    account?: IAccount;
    firebase?: FirebaseLogin;
}
