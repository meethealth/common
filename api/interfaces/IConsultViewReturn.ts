import {IConsultItem} from "../../database/interfaces/IConsultItem";

export interface IConsultViewReturn {

    consultItem?: IConsultItem;

}
