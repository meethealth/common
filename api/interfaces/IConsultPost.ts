import {IConsultItem} from "../../database/interfaces/IConsultItem";

export interface IConsultPost {
    patientId: number;
    specialtyId: string;
    consult: IConsultItem;
    consultGroupId?: number;
    targetDoctorId?: number;
}
