import {IConsultItem} from "../../database/interfaces/IConsultItem";

export interface IConsultListReturn {

    isEndOfList?: boolean;
    consultsList?: IConsultItem[];
}
