export interface ILoginPost {
    email: string;
    password: string;
}

export interface ILoginFacebookPost {
    token: string;
}
